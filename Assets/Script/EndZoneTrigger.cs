﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndZoneTrigger : MonoBehaviour
{
    public LevelControll levelControllScript; // a level controll var

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player")) //  if the player has reached the end zone
        {
            levelControllScript.LevelWon(); // calls the level controll to activate the win level menu
        }
    }


}
