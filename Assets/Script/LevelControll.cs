﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelControll : MonoBehaviour
{
    public Text StarShow; // star display text var
    public int StarAmount = 0; // star smount var
    public GameObject WinMenuUI; // the win menu UI var
    public GameObject PauseMenuUI; // the pause menu UI var
    public GameObject LoseMenuUI; // the Lose menu UI var
    public bool GameIsPaused = false; // game is paused bool, set to false by default
    public GameObject Player; // player gameobject var

    private void Start()
    {
        Time.timeScale = 1; // resumes the game cycle
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P) && !GameIsPaused) // if the player presses the "P" key and the game is running
        {
            PauseGame(); // calls pauseGame function
        }
        if (Input.GetKeyDown(KeyCode.R) && GameIsPaused) // if the player presses the "R" key and the game is paused
        {
            ResumeGame(); // calls the ResumeGame function
        }

        if(Player.transform.position.y<=-10) //  if the player y position is below -10
        {
            LoseMenuUI.SetActive(true); // activets the lose menu ui
            Time.timeScale = 0; // stops the game cycle
        }

        StarShow.text = "you have " + StarAmount + " stars";  // display on the screen the amount of stars you have
    }

    public void AddStar()
    {
        StarAmount++; // adds a star to the star count
    }


    public void LevelWon()
    {
        WinMenuUI.SetActive(true); // activates the win menu ui
        Time.timeScale = 0; // stops the game cycle
    }

    public void PauseGame()
    {

        Time.timeScale = 0; // stops the game cycle
        PauseMenuUI.SetActive(true); // activates the pause menu
        GameIsPaused = true; // changes the gameisstopped bool to true
    }
    public void ResumeGame()
    {
        Time.timeScale = 1; // continuse the game cycle
        PauseMenuUI.SetActive(false); // turns off the pause menu
        GameIsPaused = false; // changes the gameisstopped bool to false
    }
    public void RestartGame()
    {
        Time.timeScale = 1; // continuse the game cycle
        GameIsPaused = false; // changes the gameisstopped bool to false
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); // calls the scene manager to reload the scene

    }
    public void MainMenu()
    {
        Time.timeScale = 1; // continuse the game cycle
        SceneManager.LoadScene(0); // calls the scene manager to load the main mneu
    }
    public void GoToNextLevel()
    {
        Time.timeScale = 1; // continuse the game cycle
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); // calls the scene manager to load the next level
    }
}
