﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarFinder : MonoBehaviour
{
    public LevelControll levelControl; // the level controll var

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player")) // if the player collided with the star
        {
            levelControl.AddStar(); // telling the level controll to add a star to the list 
            Destroy(gameObject); // destroys the current star
        }
    }

}
