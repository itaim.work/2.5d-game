﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerComtroller : MonoBehaviour
{

    public CharacterController CharacterController; // a character controller var

    public float speed = 10f; // a default speed var

    public GameObject groundCheck; //  a ground check object for the jump check

    public LayerMask LayerMask; // a layer mask var for the jump check

    public bool IsGrounded; //  set to true of the player is grounded

    public Vector3 Velocity; // a velocity vector3 for for the physics application to the player

    public float Gravity = -9.81f; // a gravity var

    public float JumpHeight = 5f; //  the jump height var

    public float CheckRadius = 0.4f; // a radius for the check sphere for the "is grounded" check


    // Update is called once per frame
    void Update()
    {

        IsGrounded = Physics.CheckSphere(groundCheck.transform.position, CheckRadius, LayerMask); //  creates a sphere around the groundcheck location, and sets the is grounded to true if the Ground Layer is in the sphere

        if (Velocity.y >= -10) //  if the player is in the air, and the velocity is high
        {
            Velocity.y += Time.deltaTime * Gravity; // if true lowers the velocity
        }

        float HorizontalInput = Input.GetAxis("Horizontal"); // takes input from the "a" & "d" keys
         
        Vector3 MoveDirection = new Vector3(0f, 0f, HorizontalInput); // sets the horizontal input to the move direction 

        if (Input.GetButtonDown("Jump") && IsGrounded) // if the user presses space bar and the player is on the ground
        {
            Velocity.y = Mathf.Sqrt(JumpHeight * 2f - Gravity); // sets the player velocity for the jump
        }

        CharacterController.Move(MoveDirection * speed * Time.deltaTime); // moves the player on the surfece

        CharacterController.Move(Velocity * Time.deltaTime); // moves the player up || down according to the velocity
    }

}
