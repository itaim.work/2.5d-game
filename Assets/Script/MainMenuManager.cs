﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    public GameObject MainMenuUI; // main menu ui var
    public GameObject LevelPickUI;  // level menu ui var

    public void OpenLevelMenu()
    {
        MainMenuUI.SetActive(false); // turn off the main menu ui

        LevelPickUI.SetActive(true); // turn on the level menu ui
    }
    public void Quit()
    {
        Application.Quit(); // closes the game
    }
    public void Back()
    {
        MainMenuUI.SetActive(true); // turn on the main menu ui

        LevelPickUI.SetActive(false); // turn off the level menu
    }
    public void StartLevel_1()
    {
        SceneManager.LoadScene(1); // calls the scene manager to load the first level
    }

    public void startLevel_2()
    {
        SceneManager.LoadScene(2); // calls the scene manager to load the seconed level
    }

    public void Startlevel_3()
    {
        SceneManager.LoadScene(3); // calls the scene manager to load the third level
    }
}
